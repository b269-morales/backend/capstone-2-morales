const Order = require("../models/Order.js");

const bcrypt = require("bcrypt");

const auth = require("../auth");

module.exports.getAllOrders = (data) => {

	if (data.isAdmin) {
		return Order.find({}).then(result => {
			return result;
		})
	}
	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) => {
		return {value}
	});
};
