const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const orderController = require("../controllers/orderController.js");


const auth = require("../auth");




router.get("/allOrders", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	orderController.getAllOrders(data).then(
		resultFromController => res.send(resultFromController)
		);
});






/*=============================*/
module.exports = router;
/*=============================*/